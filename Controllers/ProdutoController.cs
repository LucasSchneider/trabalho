using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mvc_crud.Data;
using mvc_crud.Models.DB;

namespace mvc_crud.Controllers
{
    public class ProdutoController : Controller
    {
        private readonly ILogger<ProdutoController> _logger;
        private readonly appContext _context;
        public ProdutoController(ILogger<ProdutoController> logger, appContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            IList<Produto> produtos = _context.Produtos.ToList();
            return View(produtos);
        }

    }
    
}
