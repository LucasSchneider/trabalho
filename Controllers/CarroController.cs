using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mvc_crud.Data;
using mvc_crud.Models.DB;

namespace mvc_crud.Controllers
{
    public class CarroController : Controller
    {
        private readonly ILogger<CarroController> _logger;
        private readonly appContext _context;
        public CarroController(ILogger<CarroController> logger, appContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            IList<Carro> carros = _context.Carros.ToList();
            return View(carros);
        }

    }
    
}
